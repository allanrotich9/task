<?php
return [
    'adminEmail' => 'admin@cytonn.com',
    'supportEmail' => 'support@cytonn.com',
    'tasksEmail' => 'tasks@cytonn.com',
    'user.passwordResetTokenExpire' => 3600,
];
