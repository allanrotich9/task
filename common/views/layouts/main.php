<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\assets\AdminLteAsset;
use common\assets\CdnAsset;
use common\widgets\Alert;

CdnAsset::register($this);
AdminLteAsset::register($this);
$user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one(); 
?>
<?php $this->beginPage() 
       
 ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.svg" type="image/x-icon" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper" >
    <header class="main-header">

        <a href="<?= Yii::$app->homeUrl ?>" class="logo">
            <span class="logo-mini"><b>CI</b></span>
            <span class="logo-lg">Cytonn</span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">0</span>
                        </a>
                        <ul class="dropdown-menu">
                           
                        </ul>
                    </li>
                    <!-- /.messages-menu -->

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">0</span>
                        </a>
                        
                    </li>
                    <!-- Tasks Menu -->
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">0</span>
                        </a>
                        
                    </li>

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?= Html::img('@web/img/Admin.png', ['class' => 'user-image', 'alt' => 'User Image']) ?>
                            <span class="hidden-xs"><?php echo $user->name?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <?= Html::img('@web/img/Admin.png', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
                                <p>
                                    <?php echo $user->username?>
                                   
                                </p>
                            </li>
                            <li class="user-body">
                               
                            </li>
                            <li class="user-footer">
                               
                                <div class="pull-right">
                                    <?= Html::a('Sign out', ['/site/logout'], [
                                        'class' => 'btn btn-default btn-flat',
                                        'data' => [
                                            'method' => 'post',
                                            'params' => [
                                                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">

        <section class="sidebar">

            <div class="user-panel">
                <div class="pull-left image">
                    <?= Html::img('@web/img/admin_.png', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
                </div>
                <div class="pull-left info">
                    <p><?php echo $user->name?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <?php if (isset($this->blocks['sidebar-menu'])) : ?>
                <?= $this->blocks['sidebar-menu'] ?>
            <?php endif; ?>

        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">

            <?php
            $title = ArrayHelper::getValue($this->params, 'title');
            $description = ArrayHelper::getValue($this->params, 'description');
            $breadcrumbs = ArrayHelper::getValue($this->params, 'breadcrumbs', []);
            ?>

            <?php if ($title || $description) : ?>
                <?= Html::tag('h1', trim($title . ' ' . Html::tag($description ? 'small' : null, $description))) ?>
            <?php endif; ?>

            <?= Breadcrumbs::widget([ 'links' => $breadcrumbs ]) ?>

        </section>

        <section class="content"style="background-color: white">

            <?= Alert::widget() ?>
            <?= $content ?>

        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Version 1.1
        </div>
        <strong>Copyright &copy; 2018 <a href="http://cytonn.com/">Cytonn Investments</a>.</strong> All rights reserved.
    </footer>

    <?php //= $this->render('@common/views/layouts/control-sidebar') ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
