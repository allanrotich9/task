<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "taskdocument".
 *
 * @property integer $id
 * @property integer $taskid
 * @property string $path
 *
 * @property Task $task
 */
class Taskdocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskdocument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskid', 'path'], 'required'],
            [['taskid'], 'integer'],
            [['path'], 'string', 'max' => 500],
            [['path', 'taskid'], 'unique', 'targetAttribute' => ['path', 'taskid'], 'message' => 'The combination of Taskid and Path has already been taken.'],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'taskid' => 'Taskid',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskid']);
    }
}
