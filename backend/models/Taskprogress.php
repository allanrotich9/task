<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "taskprogress".
 *
 * @property integer $id
 * @property string $date
 * @property integer $taskid
 * @property integer $userid
 * @property string $message
 *
 * @property Task $task
 * @property User $user
 */
class Taskprogress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskprogress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'taskid', 'userid', 'message'], 'required'],
            [['date'], 'safe'],
            [['taskid', 'userid'], 'integer'],
            [['message'], 'string', 'max' => 500],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'id']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'taskid' => 'Taskid',
            'userid' => 'Userid',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
