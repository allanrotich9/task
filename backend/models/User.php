<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password
 * @property integer $departmentid
 * @property string $joblevel
 *
 * @property Reminder[] $reminders
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property Taskprogress[] $taskprogresses
 * @property Taskuser[] $taskusers
 * @property Task[] $tasks1
 * @property Department $department
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'email', 'phone', 'password', 'departmentid', 'joblevel'], 'required'],
            [['status', 'created_at', 'updated_at', 'departmentid'], 'integer'],
            [['username', 'email', 'phone'], 'string', 'max' => 30],
            [['name', 'password_reset_token', 'auth_key', 'password'], 'string', 'max' => 50],
            [['password_hash'], 'string', 'max' => 60],
            [['joblevel'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['phone'], 'unique'],
            [['departmentid'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['departmentid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'departmentid' => 'Departmentid',
            'joblevel' => 'Joblevel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminders()
    {
        return $this->hasMany(Reminder::className(), ['userid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['assignedtoid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks0()
    {
        return $this->hasMany(Task::className(), ['createdbyid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskprogresses()
    {
        return $this->hasMany(Taskprogress::className(), ['userid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskusers()
    {
        return $this->hasMany(Taskuser::className(), ['userid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks1()
    {
        return $this->hasMany(Task::className(), ['id' => 'taskid'])->viaTable('taskuser', ['userid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'departmentid']);
    }
}
