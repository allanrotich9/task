<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "taskdepartment".
 *
 * @property integer $id
 * @property integer $departmentid
 * @property integer $taskid
 *
 * @property Task $task
 * @property Department $department
 */
class Taskdepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskdepartment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['departmentid', 'taskid'], 'required'],
            [['departmentid', 'taskid'], 'integer'],
            [['departmentid', 'taskid'], 'unique', 'targetAttribute' => ['departmentid', 'taskid'], 'message' => 'The combination of Departmentid and Taskid has already been taken.'],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'id']],
            [['departmentid'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['departmentid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'departmentid' => 'Departmentid',
            'taskid' => 'Taskid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'departmentid']);
    }
}
