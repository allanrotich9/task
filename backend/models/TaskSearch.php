<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Task;

/**
 * TaskSearch represents the model behind the search form about `backend\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public $department;
    public function rules()
    {
        return [
            [['id', 'priority', 'accesslevel', 'assignedtoid', 'createdbyid', 'categoryid', 'status'], 'integer'],
            [['name', 'description', 'date', 'duedate','department'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
        if($user->joblevel==2){
           //$dataProvider->query->leftJoin('taskdepartment','taskdepartment.taskid=task.id')->where(['=','taskdepartment.taskid',2]);
            $dataProvider->query->leftJoin('taskdepartment','taskdepartment.taskid=task.id')->leftJoin('taskuser','taskuser.taskid=task.id')->where(['=','taskdepartment.departmentid',$user->departmentid])->orWhere(['=','taskuser.userid',$user->id])->orWhere(['=','task.assignedtoid',$user->id])->orWhere(['=','task.createdbyid',$user->id])->orWhere(['=','taskdepartment.departmentid',$user->departmentid]);
        }else{
            //$dataProvider->query->joinWith('taskdepartments')->where(['=','taskdepartment.taskid','task.id'])->andWhere(['=','taskdepartment.departmentid',$user->departmentid]);
            $dataProvider->query->leftJoin('taskuser','taskuser.taskid=task.id')->leftJoin('taskdepartment','taskdepartment.taskid=task.id')->where(['=','task.assignedtoid',$user->id])->orWhere(['=','taskdepartment.departmentid',$user->departmentid])->orWhere(['=','taskuser.userid',$user->id])->orWhere(['=','task.createdbyid',$user->id])->andWhere(['or',['task.accesslevel'=>1],['and',['task.accesslevel'=>2],['or',['task.assignedtoid'=>$user->id],['task.createdbyid'=>$user->id],['taskuser.userid'=>$user->id]]]]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'priority' => $this->priority,
            'date' => $this->date,
            'accesslevel' => $this->accesslevel,
            'assignedtoid' => $this->assignedtoid,
            'createdbyid' => $this->createdbyid,
            'categoryid' => $this->categoryid,
            'duedate' => $this->duedate,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);
      $query->andFilterWhere(['=','taskdepartment.departmentid',$this->department]);
        return $dataProvider;
    }
}
