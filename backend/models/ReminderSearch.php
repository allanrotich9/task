<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Reminder;

/**
 * ReminderSearch represents the model behind the search form about `backend\models\Reminder`.
 */
class ReminderSearch extends Reminder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userid', 'taskid', 'noofdays', 'reminderstatus', 'status'], 'integer'],
            [['currentdate', 'lastreminderdate', 'nextreminderdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reminder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'userid' => $this->userid,
            'taskid' => $this->taskid,
            'currentdate' => $this->currentdate,
            'lastreminderdate' => $this->lastreminderdate,
            'noofdays' => $this->noofdays,
            'nextreminderdate' => $this->nextreminderdate,
            'reminderstatus' => $this->reminderstatus,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
