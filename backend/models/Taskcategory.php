<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "taskcategory".
 *
 * @property integer $id
 * @property string $category
 * @property integer $categoryid
 *
 * @property Task[] $tasks
 * @property Taskcategory $category0
 * @property Taskcategory[] $taskcategories
 */
class Taskcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['categoryid'], 'integer'],
            [['category'], 'string', 'max' => 50],
            [['category'], 'unique'],
            [['categoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Taskcategory::className(), 'targetAttribute' => ['categoryid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'categoryid' => 'Sub Category Of',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['categoryid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(Taskcategory::className(), ['id' => 'categoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskcategories()
    {
        return $this->hasMany(Taskcategory::className(), ['categoryid' => 'id']);
    }
}
