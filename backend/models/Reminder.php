<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "reminder".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $taskid
 * @property string $currentdate
 * @property string $lastreminderdate
 * @property integer $noofdays
 * @property string $nextreminderdate
 * @property integer $reminderstatus
 * @property integer $status
 *
 * @property Task $task
 * @property User $user
 */
class Reminder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reminder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'noofdays', 'nextreminderdate'], 'required'],
            [['userid', 'taskid', 'noofdays', 'reminderstatus', 'status'], 'integer'],
            [['currentdate', 'lastreminderdate', 'nextreminderdate'], 'safe'],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'id']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'taskid' => 'Taskid',
            'currentdate' => 'Currentdate',
            'lastreminderdate' => 'Lastreminderdate',
            'noofdays' => 'Number Of Days Between Reminders',
            'nextreminderdate' => 'Nextreminderdate',
            'reminderstatus' => 'Reminderstatus',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
