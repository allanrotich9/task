<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "taskuser".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $taskid
 *
 * @property Task $task
 * @property User $user
 */
class Taskuser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taskuser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'taskid'], 'required'],
            [['userid', 'taskid'], 'integer'],
            [['userid', 'taskid'], 'unique', 'targetAttribute' => ['userid', 'taskid'], 'message' => 'The combination of Userid and Taskid has already been taken.'],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'id']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'taskid' => 'Taskid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
