<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "userrecord".
 *
 * @property integer $id
 * @property string $joblevel
 * @property string $employeename
 * @property string $employeedepartment
 * @property string $employeeemail
 */
class Userrecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userrecord';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['joblevel', 'employeename', 'employeedepartment', 'employeeemail'], 'required'],
            [['employeeemail'],'email'],
            [['joblevel', 'employeename', 'employeedepartment', 'employeeemail'], 'string', 'max' => 100],
            [['employeeemail'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'joblevel' => 'Job Level',
            'employeename' => 'Employee Name',
            'employeedepartment' => 'Employee Department',
            'employeeemail' => 'Employee Email',
        ];
    }
}
