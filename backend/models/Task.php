<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $priority
 * @property string $date
 * @property integer $accesslevel
 * @property string $taskname
 * @property integer $assignedtoid
 * @property integer $createdbyid
 * @property integer $categoryid
 * @property string $duedate
 * @property integer $status
 *
 * @property Reminder[] $reminders
 * @property User $assignedto
 * @property User $createdby
 * @property Taskcategory $category
 * @property Taskdepartment[] $taskdepartments
 * @property Department[] $departments
 * @property Taskdocument[] $taskdocuments
 * @property Taskprogress[] $taskprogresses
 * @property Taskuser[] $taskusers
 * @property User[] $users
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $department,$user,$document,$docFiles;
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'priority', 'accesslevel', 'assignedtoid', 'categoryid', 'duedate'], 'required'],
            [['priority', 'accesslevel', 'assignedtoid', 'createdbyid', 'categoryid', 'status'], 'integer'],
            [['date', 'duedate','department','user','document'], 'safe'],
            [['docFiles'], 'file', 'maxFiles' => 10,'maxSize' => 1900000,],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 500],
            [['assignedtoid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['assignedtoid' => 'id']],
            [['createdbyid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['createdbyid' => 'id']],
            [['categoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Taskcategory::className(), 'targetAttribute' => ['categoryid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Task Name',
            'description' => 'Task Description',
            'priority' => 'Priority',
            'date' => 'Date',
            'accesslevel' => 'Accesslevel',
            'assignedtoid' => 'Assigned To',
            'createdbyid' => 'Created By',
            'categoryid' => 'Task Category',
            'duedate' => 'Due Date',
            'status' => 'Status',
            'docFiles' => 'Documents',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReminders()
    {
        return $this->hasMany(Reminder::className(), ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedto()
    {
        return $this->hasOne(User::className(), ['id' => 'assignedtoid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby()
    {
        return $this->hasOne(User::className(), ['id' => 'createdbyid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Taskcategory::className(), ['id' => 'categoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskdepartments()
    {
        return $this->hasMany(Taskdepartment::className(), ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasMany(Department::className(), ['id' => 'departmentid'])->viaTable('taskdepartment', ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskdocuments()
    {
        return $this->hasMany(Taskdocument::className(), ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskprogresses()
    {
        return $this->hasMany(Taskprogress::className(), ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskusers()
    {
        return $this->hasMany(Taskuser::className(), ['taskid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'userid'])->viaTable('taskuser', ['taskid' => 'id']);
    }
    
}
