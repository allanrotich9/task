<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Taskprogress */

$this->title = 'New Task Message';
$this->params['breadcrumbs'][] = ['label' => 'Task Message', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskprogress-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
