<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskprogressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Task Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskprogress-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    $taskid=($taskid+10000)/20;
    ?>

     <p style="text-align: right">
        <?= Html::a('New Task Message', ['create','id'=>$taskid], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date',
            'taskid',
            'userid',
            'message',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
