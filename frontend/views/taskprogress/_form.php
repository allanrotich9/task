<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\Taskprogress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taskprogress-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), ['pluginOptions' => [
          'format' => 'yyyy-m-dd',
        'todayHighlight' => true
     ]]) ?>

    <?= $form->field($model, 'message')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add Message' : 'Update Message', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
