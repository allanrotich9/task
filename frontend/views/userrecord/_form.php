<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Userrecord */
/* @var $form yii\widgets\ActiveForm */

$departments= ArrayHelper::map(backend\models\Department::find()->orderBy('department')->all(), 'id', 'department') ;
?>

<div class="userrecord-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= Html::a('Add Department',['/department/new'],['class' =>'btn btn-success'])?> 
    <?= $form->field($model, 'employeename')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'employeeemail')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'joblevel')->dropDownList([1=>'Junior',2=>'Supervisor'],['prompt'=>'Select...','id'=>'serviceid']);?>
     <?= $form->field($model, 'employeedepartment')->dropDownList($departments,['prompt'=>'Select (add department if not in list)...','id'=>'serviceid']);?>
     
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add Employee' : 'Update Employee', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
