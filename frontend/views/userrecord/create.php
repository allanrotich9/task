<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Userrecord */

$this->title = 'Add Employee';
$this->params['breadcrumbs'][] = ['label' => 'Userrecords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userrecord-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
