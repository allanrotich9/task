<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Taskdepartment */

$this->title = 'Create Taskdepartment';
$this->params['breadcrumbs'][] = ['label' => 'Taskdepartments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskdepartment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
