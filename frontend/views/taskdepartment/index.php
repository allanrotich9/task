<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskdepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taskdepartments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskdepartment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Taskdepartment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'departmentid',
            'taskid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
