<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\ArrayHelper;
use common\widgets\Nav;

 $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
?>
<?php $this->beginContent('@common/views/layouts/main.php') ?>

    <?php $this->beginBlock('sidebar-menu') ?>

        <?= Nav::widget([
            'items' => [
                [
                    'label' => 'Home',
                    'icon' => 'home',
                    'url' => ['/site/index'],
                ],
                [
                    'label' => 'Tasks',
                    'icon' => 'tasks',
                    'items' => [
                              [
                                   'label' => 'Categories',
                                   'icon' => 'list',
                                   'url' => ['/taskcategory/'],
                                ],
                                [
                                   'label' => 'Tasks',
                                   'icon' => 'tasks',
                                   'url' => ['/task/'],
                                ],
                               
                         ],
                ],
                [
                    'label' => 'Employees(for Test purpose)',
                    'icon' => 'user',
                    'url' => ['/userrecord/'],
                     'visible' => $user->email=='ct@cytonn.com'
                ],
              
                [
                    'label' => 'Logout (' . ArrayHelper::getValue(Yii::$app->user, 'identity.username') . ')',
                    'icon' => 'sign-out',
                    'url' => ['/site/logout'],
                    'linkOptions' => [
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                Yii::$app->request->csrfParam => Yii::$app->request->csrfToken,
                            ],
                        ],
                    ],
                ],
             
            ]
        ]) ?>

    <?php $this->endBlock() ?>

    <?= $content ?>

<?php $this->endContent() ?>
