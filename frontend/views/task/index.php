<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\grid\ActionColumn;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
     
     <p style="text-align: right">
        <?= Html::a('New Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
   <?php 
   
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'label'=>'Task Type',
                'format' => 'raw',
                'attribute' => 'accesslevel', 
                'filter' => Html::activeDropDownList($searchModel, 'accesslevel', [1=>'public', 2=>'private'],['class'=>'form-control','prompt' => 'All']),
             'value' => function ($data) {
                 
                 if($data->accesslevel==1){
                     return 'Public';
                 }
                  return 'Private';
             },
                     
          ],
           
            
            ['class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'label'=>'Priority',
                'format' => 'raw',
             'value' => function ($data) {
                 if($data->priority==1){
                     return 'Urgent AND important';
                 }else if($data->priority==2){
                     return 'Important NOT urgent';
                 }else if($data->priority==3){
                     return 'Urgent NOT important';
                 }
                  return 'NOT urgent OR important';
             },
          ],
            
            'duedate',
            [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'label'=>'My Tasks',
                'format' => 'raw',
               'attribute' => 'accesslevel', 
                'filter' => Html::activeDropDownList($searchModel, 'status', [1=>'Open', 2=>'Completed'],['class'=>'form-control','prompt' => 'All Tasks']),
                'value' => function ($data) {
                 
                 if($data->status==1){
                     if(date("Y-m-d")>$data->duedate){
                         return 'Overdue';
                     }
                     return 'ongoing';
                 }
                  return 'Completed';
             },
          ],
            [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'format' => 'raw',
             'value' => function ($data) {
                 $id=($data->id+10000)/20;
                 if($data->status==1){
                      return Html::a('Reminder', ['reminder/index', 'id' => $id], ['class' => 'btn btn-success']);
                 }
             
             },
          ],
            [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'format' => 'raw',
             'value' => function ($data) {
                 $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                 $department=  \backend\models\Taskdepartment::find()->where(['taskid' => $data->id])->andWhere(['departmentid' => $user->departmentid])->one();
                 if(isset($department)){
                     if($user->joblevel==2 && $data->accesslevel==2){
                       return Html::a('Messages', ['taskprogress/index', 'id' => $data->id], ['class' => 'btn btn-success']);
                   }else{
                     return '';
                 }
                 }else{
                     return '';
                 }
                 
              
             },
          ],
                     [
            'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                'format' => 'raw',
             'value' => function ($data) {
                 $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                 $userassignee =  \backend\models\User::find()->where(['id' => $data->assignedtoid])->one();
                 if(($data->assignedtoid==$user->id || ($user->joblevel==2 && $user->departmentid=$userassignee->departmentid))  && date("Y-m-d")>$data->duedate){
                     return Html::a('Re-assign', ['update', 'id' => $data->id], ['class' => 'btn btn-success']);
                 }else{
                     return '';
                 }
              
             },
          ],
            // 'accesslevel',
            // 'assignedtoid',
            //'createdbyid',
            // 'categoryid',
            // 'duedate',
            // 'status',

           
            [
              'class' => 'yii\grid\DataColumn',
            'format' => 'raw',
              'value' => function ($data) {
                 $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                 $usercreator =  \backend\models\User::find()->where(['id' => $data->createdbyid])->one();
              if($data->createdbyid==$user->id || ($user->joblevel==2 && $user->departmentid=$usercreator->departmentid)){
                  return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $data->id]).Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $data->id]); 
              }else{
                  return '';
              }
              },
          ],
        ];
               $fullExportMenu = ExportMenu::widget([
    'dataProvider' => $dataProvider,
        
     'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns' => $gridColumns,
    'target' => ExportMenu::TARGET_BLANK,
    'fontAwesome' => true,
         'pjax'=>true,
    'asDropdown' => false, // this is important for this case so we just need to get a HTML list    
    'dropdownOptions' => [
        'label' => '<i class="glyphicon glyphicon-export"></i> Full'
    ],
]);
        
     echo GridView::widget([
    'dataProvider' => $dataProvider,
       'filterModel'=>$dataProvider,
    'showPageSummary'=>false,
    'striped'=>true,
    'hover'=>true,
       'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns' => $gridColumns,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  Tasks</h3>',
    ],
    // the toolbar setting is default
    'toolbar' => [
        '{export}',
        
         
         
    ],
    // configure your GRID inbuilt export dropdown to include additional items
    'export' => [
        'fontAwesome' => true,
        'itemsAfter'=> [
            '<li role="presentation" class="divider"></li>',
            '<li class="dropdown-header">Export All Data</li>',
            $fullExportMenu
        ]
    ],
]); 
        ?>

</div>
