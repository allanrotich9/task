<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\TaskSearch */
/* @var $form yii\widgets\ActiveForm */
$department= ArrayHelper::map(backend\models\Department::find()->orderBy('department')->all(), 'id', 'department') ;
?>

<div class="task-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<div class="row">
    <div class="col-md-6">
         <?= $form->field($model, 'department')->dropDownList($department,['onchange'=>'this.form.submit()','prompt'=>'All']);?>
    </div>
    <div class="col-md-6">
        
    </div>
</div>
  

    <?php ActiveForm::end(); ?>

</div>
