<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Task */
/* @var $form yii\widgets\ActiveForm */
$users= ArrayHelper::map(backend\models\User::find()->orderBy('email')->all(), 'id', 'name','email') ;
$category= ArrayHelper::map(backend\models\Taskcategory::find()->orderBy('category')->all(), 'id', 'category') ;

$allfiles = array();
$docs = ArrayHelper::map(\backend\models\Taskdocument::find()->select(['*','id as id', 'path as name'])->where(['taskid' => $model->id])->asArray()->all(),'id','name'); 

foreach($docs as $do){
     $image_url =Yii::$app->request->baseUrl."/uploads/".$do;
     array_push($allfiles,  Html::img($image_url, ['width'=>'200']));
}
     

?>

<div class="task-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'priority')->dropDownList([1=>'Urgent AND important', 2=>'Important NOT urgent', 3=>'Urgent NOT important', 4=>'NOT urgent OR important'],['prompt'=>'Select...']);?>
    
    <?= $form->field($model, 'accesslevel')->dropDownList([1=>'public', 2=>'private'],['prompt'=>'Select...']);?>
    
    <?= $form->field($model, 'assignedtoid')->dropDownList($users,['prompt'=>'Select...']);?>
    
     <?= $form->field($model, 'categoryid')->dropDownList($category,['prompt'=>'Select...']);?>
    <?php 
   $data = ArrayHelper::map(\backend\models\Department::find()->select(['*','id as id', 'CONCAT(department,"_",id) as name'])->asArray()->all(),'id','name'); 

foreach($data as $d)
     $row[]=$d;
if(isset( $row)){
echo $form->field($model, 'department')->widget(Select2::classname(), [
   'language' => 'en',
   'name' => 'id[]',
    'attribute'=>'department',
   'options' => ['placeholder' => ''],
   'pluginOptions' => [
        'tags' => $row,
        'allowClear' => true,
        'multiple' => true
    ],
])->label('Select department(s)');
}
?>
     <?php 
   $data2 = ArrayHelper::map(\backend\models\User::find()->select(['*','id as id', 'CONCAT(username,"(",email,")","_",id) as name'])->asArray()->all(),'id','name'); 

foreach($data2 as $d2)
     $row2[]=$d2;
if(isset( $row)){
echo $form->field($model, 'user')->widget(Select2::classname(), [
   'language' => 'en',
   'name' => 'id[]',
    'attribute'=>'user',
   'options' => ['placeholder' => ''],
   'pluginOptions' => [
        'tags' => $row2,
        'allowClear' => true,
        'multiple' => true
    ],
])->label('Select User Group(s)');
}
?>
     
    <?= $form->field($model, 'docFiles[]')->widget(FileInput::classname(), [
              'pluginOptions'=>['allowedFileExtensions'=>['jpg','png','pdf'],'showUpload' => true,'maxFileCount' => 10,
            
                  ],
         'attribute' => 'attachment_1[]',
         
    'options' => ['multiple' => true],
        'pluginOptions' => [
        'initialPreview'=>$allfiles,
        'overwriteInitial'=>true
    ]
          ]);   ?>
    
     <?= $form->field($model, 'duedate')->widget(DatePicker::classname(), ['pluginOptions' => [
          'format' => 'yyyy-m-dd',
        'todayHighlight' => true
     ]]) ?>
    <div class="form-group">
        
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']).'          '.Html::a($model->isNewRecord ?null :'Project Completed', ['complete', 'id' => $model->id], ['class' => $model->isNewRecord ?null:'btn btn-success']); ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
