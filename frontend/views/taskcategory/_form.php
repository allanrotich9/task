<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Taskcategory */
/* @var $form yii\widgets\ActiveForm */
$category= ArrayHelper::map(backend\models\Taskcategory::find()->orderBy('category')->all(), 'id', 'category') ;
?>

<div class="taskcategory-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'categoryid')->dropDownList($category,['prompt'=>'Select...']);?>
     
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
