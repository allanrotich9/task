<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskcategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Task Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskcategory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Task Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category',
            'categoryid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
