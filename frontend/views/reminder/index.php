<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReminderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Task Reminders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminder-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    $taskid=($taskid+10000)/20;
    ?>

    <p style="text-align: right">
        <?= Html::a('New Task Reminder', ['create','id'=>$taskid], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'userid',
            'taskid',
            'currentdate',
            'lastreminderdate',
            // 'noofdays',
            // 'nextreminderdate',
            // 'reminderstatus',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
