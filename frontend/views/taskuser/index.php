<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TaskuserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taskusers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taskuser-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Taskuser', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'userid',
            'taskid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
