<?php

namespace frontend\controllers;

use Yii;
use backend\models\Task;
use backend\models\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $data = ArrayHelper::map(\backend\models\Reminder::find()->select(['*','id as id', 'id as name'])->where(['nextreminderdate'=>date("Y-m-d")])->asArray()->all(),'id','name'); 
        foreach($data as $d){
            $reminder = \backend\models\Reminder::find()->where(['id' => $d])->one();
            $userto =  \backend\models\User::find()->where(['id' => $reminder->userid])->one();
            $task =  \backend\models\Task::find()->where(['id' => $reminder->taskid])->one();
            $reminderModel= \backend\models\Reminder::findOne($d);
            if(date("Y-m-d")<=$task->duedate && $task->status==1){
                $this->sendEmail($userto->email, 'Task reminder '.$task->description, 'Task('.$task->name.')');
                $reminderModel->lastreminderdate=$reminderModel->nextreminderdate;
                $reminderModel->nextreminderdate=date('Y-m-d', strtotime($reminderModel->nextreminderdate. ' +'. $reminderModel->noofdays .' days'));
                
                $reminderModel->save(); 
            }
            
        }
          return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();
       $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
        if ($model->load(Yii::$app->request->post())) {
            $model->createdbyid=$user->id;
            $model->status=1;
            $model->date=date("Y-m-d");
            if($model->save()){
                $userassignedto =  \backend\models\User::find()->where(['id' => $model->assignedtoid])->one();
                $this->sendEmail($userassignedto->email, 'You have been assigned new task '.$model->description, 'New Task('.$model->name.')');
                $data= $model->department;
             if(!empty($data)){
             foreach ($data as $departmentid){
                 $pos = strrpos($departmentid, '_');
                  $id = $pos === false ? $departmentid : substr($departmentid, $pos + 1);
                 $department=new \backend\models\Taskdepartment();
                 $department->taskid= $model->id;
                 $department->departmentid= $id;
                 $department->save();
             }
             }
              $datauser= $model->user;
             if(!empty($datauser)){
             foreach ($datauser as $userid){
                 $pos = strrpos($userid, '_');
                  $id = $pos === false ? $userid : substr($userid, $pos + 1);
                 $usergroup=new \backend\models\Taskuser();
                 $usergroup->taskid= $model->id;
                 $usergroup->userid= $id;
                 if($usergroup->save()){
                     if($model->accesslevel==1){
                          $userto =  \backend\models\User::find()->where(['id' => $id])->one();
                          $this->sendEmail($userto->email, 'You have been added to user group task '.$model->description, 'New Task('.$model->name.')');
                     }
                 }
                 
             }
             }
             $model->docFiles= \yii\web\UploadedFile::getInstances($model, 'docFiles');
             if(!empty($model->docFiles)){
             foreach ($model->docFiles as $file) {
                 $document=new \backend\models\Taskdocument();
                   $rnd = rand(0,9999);
                   $filename=$rnd.'-'.$file->baseName . '.' . $file->extension;
                   $filePath = Yii::$app->basePath.'/web/uploads/'.$filename;
                   $file->saveAs($filePath,false);
                   $document->taskid=$model->id;
                   $document->path=$filename;
                   $document->save();
            }
            }
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $createdbyid=$model->createdbyid;
       $user =  \backend\models\User::find()->where(['username' => Yii::$app->user->identity->username])->one();
        $datadepartments = ArrayHelper::map(\backend\models\Taskdepartment::find()->joinWith('department')->select(['*','taskdepartment.id as id', 'CONCAT(department.department,"_",department.id) as name'])->where('taskdepartment.taskid = :taskid', [':taskid' => $id])->asArray()->all(),'id','name'); 
        if(!empty($datadepartments)){
            foreach($datadepartments as $d)
            $row[]=$d;
            $model->department=$row;
        }
        $datauser = ArrayHelper::map(\backend\models\Taskuser::find()->joinWith('user')->select(['*','taskuser.id as id', 'CONCAT(user.username,"(",user.email,")","_",user.id) as name'])->where('taskuser.taskid = :taskid', [':taskid' => $id])->asArray()->all(),'id','name'); 
        if(!empty($datauser)){
            foreach($datauser as $d)
            $row2[]=$d;
            $model->user=$row2;
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->createdbyid=$createdbyid;
            $model->status=1;
            $model->date=date("Y-m-d");
            if($model->save()){
                 $userassignedto =  \backend\models\User::find()->where(['id' => $model->assignedtoid])->one();
                $this->sendEmail($userassignedto->email, 'You have been assigned new task '.$model->description, 'New Task('.$model->name.')');
                 \backend\models\Taskdepartment::deleteAll('taskid = :taskid', [':taskid' => $id]);
                \backend\models\Taskuser::deleteAll('taskid = :taskid', [':taskid' => $id]);
                 $data= $model->department;
             if(!empty($data)){
             foreach ($data as $departmentid){
                 $pos = strrpos($departmentid, '_');
                  $id = $pos === false ? $departmentid : substr($departmentid, $pos + 1);
                 $department=new \backend\models\Taskdepartment();
                 $department->taskid= $model->id;
                 $department->departmentid= $id;
                 $department->save();
             }
             }
             $datauser= $model->user;
             if(!empty($datauser)){
             foreach ($datauser as $userid){
                 $pos = strrpos($userid, '_');
                  $id = $pos === false ? $userid : substr($userid, $pos + 1);
                 $usergroup=new \backend\models\Taskuser();
                 $usergroup->taskid= $model->id;
                 $usergroup->userid= $id;
                 if($usergroup->save()){
                     if($model->accesslevel==1){
                          $userto =  \backend\models\User::find()->where(['id' => $id])->one();
                          $this->sendEmail($userto->email, 'You have been added to user group task '.$model->description, 'New Task('.$model->name.')');
                     }
                 }
             }
             }
            $model->docFiles= \yii\web\UploadedFile::getInstances($model, 'docFiles');
            
             if(!empty($model->docFiles)){
                 \backend\models\Taskdocument::deleteAll('taskid = :taskid', [':taskid' => $id]);
             foreach ($model->docFiles as $file) {
                 $document=new \backend\models\Taskdocument();
                   $rnd = rand(0,9999);
                   $filename=$rnd.'-'.$file->baseName . '.' . $file->extension;
                   $filePath = Yii::$app->basePath.'/web/uploads/'.$filename;
                   $file->saveAs($filePath,false);
                   $document->taskid=$model->id;
                   $document->path=$filename;
                   $document->save();
            }
            }
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
   public function actionComplete($id)
    {
        $model = $this->findModel($id);
        $model->status=2;
        $model->save();
        return $this->redirect(['site/index']);
    }
    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    function sendEmail($emailto,$msg,$subject) {
        return Yii::$app
            ->mailer->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($emailto)
            ->setTextBody($msg)
            ->setSubject($subject)
            ->send();
    }
}
