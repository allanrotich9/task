<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use yii\helpers\ArrayHelper;
use borales\extensions\phoneInput\PhoneInputValidator;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $phone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\backend\models\Userrecord',
                'targetAttribute' => 'employeeemail',
                'message' => 'There is no user with such email. Contact Administrator'
            ],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['departmentid', 'safe'],
            ['password', 'required'],
            ['phone', 'required'],
            [['phone'], PhoneInputValidator::className()],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $userrecord= \backend\models\Userrecord::find()->where(['employeeemail'=>$this->email])->one();
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->joblevel = $userrecord->joblevel;
        $user->name = $userrecord->employeename;
        $user->departmentid=$userrecord->employeedepartment;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
